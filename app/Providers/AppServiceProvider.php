<?php

namespace App\Providers;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        Response::macro('api', function ($httpCode = 200, $status = 'success', $data = [], $message = '', $action = '') {
            return response()->json([
                'data' => $data,
                'message' => $message,
                'response_code' => $httpCode,
                'response_status' => $status,
                'action' => $action,
            ], $httpCode);
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
