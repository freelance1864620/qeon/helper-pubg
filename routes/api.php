<?php

use App\Models\Setting;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::any('/hello', function () {
    return response()->api(200, 'success', [], 'Hello..');
});

Route::any('/last-status-truncate', function () {
    $setting = Setting::where('key', 'truncate_data')->first();
    return response()->api(200, 'success', [], $setting->value);
});

Route::any('/last-status-is-running', function () {
    $setting = Setting::where('key', 'is_running')->first();
    return response()->api(200, 'success', [], $setting->value);
});

Route::any('/change-status-truncate', function () {
    $setting = Setting::where('key', 'truncate_data')->first();
    $newValue = ($setting->value == 'yes') ? 'no' : 'yes';
    Setting::where('key', 'truncate_data')->update(['value' => $newValue]);
    return response()->api(200, 'success', [], 'new value truncate : ' . $newValue);
});

Route::any('/change-status-is-running', function () {
    $setting = Setting::where('key', 'is_running')->first();
    $newValue = ($setting->value == 'yes') ? 'no' : 'yes';
    Setting::where('key', 'is_running')->update(['value' => $newValue]);
    return response()->api(200, 'success', [], 'new value is running : ' . $newValue);
});
